import { Component } from '@angular/core';
import { PostsService } from './services/posts.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  // CurrencyPipe
  precio: number;

  // DecimalPipe
  numero: number;

  // DatePipe
  // https://angular.io/api/common/DatePipe Para ver todas las combinaciones con este pipe
  fechaActual: Date;

  //PercentPipe
  numAleatorio: number;

  //JSONPipe
  estudiante: any;

  // Lower, Upper, Title
  texto: string;

  //SlicePipe
  animales: string[];
  minimo:number;
  maximo: number;

  // AsyncPipe
  prom: Promise<string>;
  promPosts: Promise<any[]>;

  constructor( private postsService: PostsService){
    this.precio = 198.878878;
    this.numero = 3.8819200430;
    this.fechaActual = new Date();
    this.numAleatorio = Math.random()
    this.estudiante = {
      nombre: 'Matias',
      apellidos: 'Guerrero',
      notas: [3, 4, 7, 10]
    }
    this.texto = 'En un lugar de la Mancha'
    this.animales = ['perro', 'gato', 'pájaro', 'tortuga']
    this.minimo = 0;
    this.maximo =  this.animales.length;
    this.prom = new Promise<string>((resolve, reject) => {
      setTimeout(() => resolve('Se resuelve esta promesa pasados 4 segundos'), 4000)
    });

    this.promPosts = this.postsService.getAll();
  }

  ngOnInit(){
    setInterval(() =>this.fechaActual = new Date(),1000);
  }
}
